import { IsNotEmpty, MinLength, IsInt, IsPositive } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  @IsInt()
  @IsPositive()
  price: number;
}
